﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab24GameWIthTimer
{
    public partial class Form1 : Form
    {
        bool goLeft, goRight;
        int caught = 0;
        int missed = 0;
        int speed = 8;

        Random randX = new Random();
        Random randY = new Random();

        PictureBox targetmissed = new PictureBox();


        public Form1()
        {
            InitializeComponent();
            GameTimer.Stop();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void MainGameTimer(object sender, EventArgs e)
        {
            caught_label.Text = "Caught: " + caught;
            missed_label.Text = "Missed: " + missed;
            if (goLeft == true && player.Left > 0)
            {
                player.Left -= 12;
                player.Image = Properties.Resources.TomHanksToLeft;

            }
            if (goRight == true && player.Left + player.Width < this.ClientSize.Width)
            {
                player.Left += 12;
                player.Image = Properties.Resources.TomHanks;
            }
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "target")
                {
                    x.Top += speed;


                    if (x.Top + x.Height > this.ClientSize.Height)
                    {
                        targetmissed.Image = Properties.Resources.LeoDicaprioMissed;

                        targetmissed.Location = x.Location;
                        targetmissed.Size = x.Size;
                        targetmissed.SizeMode = PictureBoxSizeMode.StretchImage;
                        targetmissed.BackColor = Color.Transparent;
                        this.Controls.Add(targetmissed);


                        x.Top = randY.Next(80, 300) * -1;
                        x.Left = randX.Next(5, this.ClientSize.Width - x.Width);

                        missed++;

                    }
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {
                        x.Top = randY.Next(80, 300) * -1;
                        x.Left = randX.Next(5, this.ClientSize.Width - x.Width);
                        caught++;
                    }
                }
            }
            if (caught > 10)
            {
                speed = 12;
            }
            if (missed > 7)
            {
                GameTimer.Stop();
                MessageBox.Show("Game over :(" + Environment.NewLine + "You have lost");
                
            }
            if (caught == 30)
            {
                GameTimer.Stop();
                MessageBox.Show("Congratulations, you have won!" + Environment.NewLine + $"You caught Frank Abignale {caught} times");
                

            }
        }

        private void KeyIsDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = true;
            }
        }

        private void KeyIsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestartGame();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameTimer.Start();
        }

        private void startToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            RestartGame();
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Use right or left buttons on your keyboard to move Carl Hanratty(Tom Hanks) and help him to catch Frank Abignale(Leo DiCaprio)");
        }

        private void RestartGame()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "target")
                {
                    x.Top = randY.Next(80, 300) * -1;
                    x.Left = randX.Next(5, this.ClientSize.Width - x.Width);
                }
            }
            player.Left = this.ClientSize.Width / 2;
            player.Image = Properties.Resources.TomHanks;

            caught = 0;
            missed = 0;
            speed = 8;

            goLeft = false;
            goRight = false;
            GameTimer.Start();


        }
    }
}
