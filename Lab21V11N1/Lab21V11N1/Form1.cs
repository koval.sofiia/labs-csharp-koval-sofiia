﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab21V11N1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void get_result_button_Click(object sender, EventArgs e)
        {
            double s = (GCD(Convert.ToDouble(a_textBox.Text), Convert.ToDouble(b_textBox.Text)) + GCD(Convert.ToDouble(a_textBox.Text), 4)) + GCD(24, Convert.ToDouble(b_textBox.Text));
            result_textBox.Text = s.ToString();
        }

        /// <summary>
        /// Function that gets GCD of two numbers
        /// </summary>
        /// <param name="a">first number</param>
        /// <param name="b">second number</param>
        /// <returns></returns>
        private double GCD(double a, double b)
        {
            double Remainder = 0;
            while (b != 0)
            {
                Remainder = a % b;
                a = b;
                b = Remainder;
            }
            return a;
        }
    }
}
