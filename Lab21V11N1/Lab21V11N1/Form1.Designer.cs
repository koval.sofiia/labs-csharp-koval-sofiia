﻿namespace Lab21V11N1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.get_result_button = new System.Windows.Forms.Button();
            this.a_textBox = new System.Windows.Forms.TextBox();
            this.b_textBox = new System.Windows.Forms.TextBox();
            this.result_textBox = new System.Windows.Forms.TextBox();
            this.a_label = new System.Windows.Forms.Label();
            this.b_label = new System.Windows.Forms.Label();
            this.result_label = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // get_result_button
            // 
            this.get_result_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.get_result_button.Location = new System.Drawing.Point(188, 192);
            this.get_result_button.Name = "get_result_button";
            this.get_result_button.Size = new System.Drawing.Size(103, 37);
            this.get_result_button.TabIndex = 0;
            this.get_result_button.Text = "calculate";
            this.get_result_button.UseVisualStyleBackColor = true;
            this.get_result_button.Click += new System.EventHandler(this.get_result_button_Click);
            // 
            // a_textBox
            // 
            this.a_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.a_textBox.Location = new System.Drawing.Point(225, 92);
            this.a_textBox.Name = "a_textBox";
            this.a_textBox.Size = new System.Drawing.Size(54, 30);
            this.a_textBox.TabIndex = 5;
            // 
            // b_textBox
            // 
            this.b_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.b_textBox.Location = new System.Drawing.Point(225, 146);
            this.b_textBox.Name = "b_textBox";
            this.b_textBox.Size = new System.Drawing.Size(54, 30);
            this.b_textBox.TabIndex = 6;
            // 
            // result_textBox
            // 
            this.result_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.result_textBox.Location = new System.Drawing.Point(225, 251);
            this.result_textBox.Name = "result_textBox";
            this.result_textBox.ReadOnly = true;
            this.result_textBox.Size = new System.Drawing.Size(54, 30);
            this.result_textBox.TabIndex = 7;
            // 
            // a_label
            // 
            this.a_label.AutoSize = true;
            this.a_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.a_label.Location = new System.Drawing.Point(194, 98);
            this.a_label.Name = "a_label";
            this.a_label.Size = new System.Drawing.Size(23, 25);
            this.a_label.TabIndex = 8;
            this.a_label.Text = "a";
            this.a_label.Click += new System.EventHandler(this.label1_Click);
            // 
            // b_label
            // 
            this.b_label.AutoSize = true;
            this.b_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.b_label.Location = new System.Drawing.Point(194, 152);
            this.b_label.Name = "b_label";
            this.b_label.Size = new System.Drawing.Size(23, 25);
            this.b_label.TabIndex = 9;
            this.b_label.Text = "b";
            // 
            // result_label
            // 
            this.result_label.AutoSize = true;
            this.result_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.result_label.Location = new System.Drawing.Point(194, 251);
            this.result_label.Name = "result_label";
            this.result_label.Size = new System.Drawing.Size(26, 25);
            this.result_label.TabIndex = 10;
            this.result_label.Text = "S";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(708, 61);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 476);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.result_label);
            this.Controls.Add(this.b_label);
            this.Controls.Add(this.a_label);
            this.Controls.Add(this.result_textBox);
            this.Controls.Add(this.b_textBox);
            this.Controls.Add(this.a_textBox);
            this.Controls.Add(this.get_result_button);
            this.Name = "Form1";
            this.Text = "Lab21V11N1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button get_result_button;
        private System.Windows.Forms.TextBox a_textBox;
        private System.Windows.Forms.TextBox b_textBox;
        private System.Windows.Forms.TextBox result_textBox;
        private System.Windows.Forms.Label a_label;
        private System.Windows.Forms.Label b_label;
        private System.Windows.Forms.Label result_label;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

