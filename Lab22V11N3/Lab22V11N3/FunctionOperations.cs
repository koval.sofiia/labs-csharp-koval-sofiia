﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms;

namespace Lab22V11N3
{
    internal class FunctionOperations
    {
        //=====поля=====
        private double A, B;

        /// <summary>
        /// конструктор з двома параметрами
        /// </summary>
        /// <param name="a">початкове значення</param>
        /// <param name="b">кінцеве значення</param>
        public FunctionOperations(double a, double b)
        {
            this.A = a; this.B = b;
        }

        /// <summary>
        /// 0.будує графік функції
        /// </summary>
        /// <param name="chart1"></param>
        public void buildFunction(Chart chart1)
        {
            chart1.Series[0].Points.Clear();

            double x = A;
            while (x <= B)
            {
                chart1.Series[0].Points.AddXY(x, Math.Sin(x) * Math.Exp(x));
                x++;
            }
        }

        /// <summary>
        /// 1.перевірка чи спадаюча
        /// </summary>
        /// <param name="chart1"></param>
        public void isDecreasing(Chart chart1)
        {

            chart1.Series[0].Points.Clear();
            double max = double.NegativeInfinity;
            double min = double.PositiveInfinity;
            for (double x = A; x <= B; x += 1)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                chart1.Series[0].Points.AddXY(x, y);
                if (y > max) max = y;
                if (y < min) min = y;
            }
            bool isDescending = max < min;
            MessageBox.Show($"The biggest value of functions is: {max}\n" +
                            $"The smallest value of function is: {min}\n" +
                            $"{(isDescending ? "Yes, decreasing" : "No, increasing")}");
        }

        /// <summary>
        /// 2.чи перетинає вісь Ох
        /// </summary>
        /// <param name="chart1"></param>
        public void doesCrossOX(Chart chart1)
        {

            chart1.Series[0].Points.Clear();
            bool intersectsX = false;
            for (double x = A; x <= B; x += 1)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                chart1.Series[0].Points.AddXY(x, y);
                if (y == 0)
                {
                    intersectsX = true;
                }
            }
            if (intersectsX)
            {
                MessageBox.Show("Yes. Function crosses axis Ox");
            }
            else
            {
                MessageBox.Show("No. Function crosses axis Oy");
            }
        }

        /// <summary>
        /// 3.середнє значення функції
        /// </summary>
        /// <param name="chart1"></param>
        public void averageValue(Chart chart1)
        {
            chart1.Series[0].Points.Clear();
            double sum = 0;
            int count = 0;
            for (double x = A; x <= B; x += 1)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                chart1.Series[0].Points.AddXY(x, y);
                sum += y;
                count++;
            }
            double average = sum / count;
            MessageBox.Show("Average value of function is: " + average.ToString());
        }
        
        /// <summary>
        /// 4.найбільше відємне значення
        /// </summary>
        /// <param name="chart1"></param>
        public void maxNegativeValue(Chart chart1)
        {
            chart1.Series[1].Points.Clear();

            double x = A;
            double minPointX = double.MaxValue;
            double minPointY = double.MinValue;

            while (x <= B)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                if (y < 0 && y > minPointY)
                {
                    minPointX = x;
                    minPointY = y;
                }
                x++;
            }
            if (minPointY > double.MinValue)
                chart1.Series[1].Points.AddXY(minPointX, minPointY);
            else
                MessageBox.Show("Doesnt have negative values");
        }

        /// <summary>
        /// 5.найменше додатнє значення
        /// </summary>
        /// <param name="chart1"></param>
        public void minPositiveValue(Chart chart1)
        {
            chart1.Series[1].Points.Clear();

            double x = A;
            double minPointX = double.MaxValue;
            double minPointY = double.MaxValue;

            while (x <= B)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                if (y > 0 && y < minPointY)
                {
                    minPointX = x;
                    minPointY = y;
                }
                x++;
            }
            if (minPointY < double.MaxValue)
                chart1.Series[1].Points.AddXY(minPointX, minPointY);
            else
                MessageBox.Show("Doesnt have positive values");
        }

        
        /// <summary>
        /// 6.перевірка чи зростаюча
        /// </summary>
        /// <param name="chart1"></param>
        public void isIncreasing(Chart chart1)
        {

            chart1.Series[0].Points.Clear();
            double max = double.NegativeInfinity;
            double min = double.PositiveInfinity;
            for (double x = A; x <= B; x += 1)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                chart1.Series[0].Points.AddXY(x, y);
                if (y > max) max = y;
                if (y < min) min = y;
            }
            bool isDescending = max > min;
            MessageBox.Show($"The biggest value of functions is: {max}\n" +
                            $"The smallest value of function is: {min}\n" +
                            $"{(isDescending ? "Yes, increasing" : "No, decreasing")}");
        }

        /// <summary>
        /// 7.наближено точка перетину з Ох
        /// </summary>
        /// <param name="chart1"></param>
        public void ApproximateIntersectionOX(Chart chart1)
        {

            chart1.Series[0].Points.Clear();

            double x = A;
            while (x <= B)
            {
                chart1.Series[0].Points.AddXY(x, Math.Sin(x) * Math.Exp(x));
                x++;
            }

            double maxY0 = 0.1;
            double minY0 = -0.1;
            double minX = double.MaxValue;
            double minY = 0;

            while (x <= B)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                if (y <= maxY0 && y >= minY0)
                {
                    minX = x;
                    minY = y;
                }
                x++;
            }

            if (x != double.MaxValue)
            {
                chart1.Series[1].Points.AddXY(minX, minY);
                MessageBox.Show($"x = {minX}");
            }
            else
                MessageBox.Show($"There is no point where function crosses Ox");
        }

        /// <summary>
        /// 8.симетрично відносно осі Ох
        /// </summary>
        /// <param name="chart1"></param>
        public void symmetricallyOX(Chart chart1)
        {
            chart1.Series[0].Points.Clear();
            double x = A;
            while (x <= B)
            {
                chart1.Series[0].Points.AddXY(x, -Math.Sin(x) * Math.Exp(x));
                x++;
            }
        }

        /// <summary>
        /// 9.симетрично відносно осі Оу
        /// </summary>
        /// <param name="chart1"></param>
        public void symmetricallyOY(Chart chart1)
        {
            chart1.Series[0].Points.Clear();

            double x = A;
            while (x <= B)
            {
                chart1.Series[0].Points.AddXY(-x, Math.Sin(x) * Math.Exp(x));
                x++;
            }
        }

        /// <summary>
        /// 10.у якому квадранті найбільше точок
        /// </summary>
        /// <param name="chart1"></param>
        public void whichQuadrant(Chart chart1)
        {
            chart1.Series[0].Points.Clear();

            double x = A;
            int quadrant1 = 0;
            int quadrant2 = 0;
            int quadrant3 = 0;
            int quadrant4 = 0;

            while (x <= B)
            {
                double y = Math.Sin(x) * Math.Exp(x);
                if (x > 0 && y > 0)
                    quadrant1++;
                else if (x < 0 && y > 0)
                    quadrant2++;
                else if (x < 0 && y < 0)
                    quadrant3++;
                else if (x > 0 && y < 0)
                    quadrant4++;
                chart1.Series[0].Points.AddXY(x, y);
                x += 1;
            }

            if (quadrant1 > quadrant2 && quadrant1 > quadrant3 && quadrant1 > quadrant4)
                MessageBox.Show("In 1 quadrant");
            else if (quadrant2 > quadrant1 && quadrant2 > quadrant3 && quadrant2 > quadrant4)
                MessageBox.Show("In 2 quadrant");
            else if (quadrant3 > quadrant1 && quadrant3 > quadrant2 && quadrant3 > quadrant4)
                MessageBox.Show("In 3 quadrant");
            else if (quadrant4 > quadrant1 && quadrant4 > quadrant2 && quadrant4 > quadrant3)
                MessageBox.Show("In 4 quadrant");
        }

        

    }
}
