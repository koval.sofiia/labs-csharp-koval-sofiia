﻿namespace Lab22V11N3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перевіркаЧиСпаднаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.точкиПеретинуЗОхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.середнєЗначенняФункціїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найменшеДодатнеЗначенняФункціїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перевіркаЧиЗростаючаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.точкаПеретинуЗОхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.симетричноВідносноОчToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.симетричноВідносноОуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.build_function_button = new System.Windows.Forms.Button();
            this.b_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.b_label = new System.Windows.Forms.Label();
            this.a_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.a_label = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.a_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.operationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1189, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(123, 26);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(120, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(123, 26);
            this.closeToolStripMenuItem.Text = "Exit";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // operationsToolStripMenuItem
            // 
            this.operationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.перевіркаЧиСпаднаToolStripMenuItem,
            this.точкиПеретинуЗОхToolStripMenuItem,
            this.середнєЗначенняФункціїToolStripMenuItem,
            this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem,
            this.найменшеДодатнеЗначенняФункціїToolStripMenuItem,
            this.перевіркаЧиЗростаючаToolStripMenuItem,
            this.точкаПеретинуЗОхToolStripMenuItem,
            this.симетричноВідносноОчToolStripMenuItem,
            this.симетричноВідносноОуToolStripMenuItem,
            this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem});
            this.operationsToolStripMenuItem.Name = "operationsToolStripMenuItem";
            this.operationsToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.operationsToolStripMenuItem.Text = "Operations";
            // 
            // перевіркаЧиСпаднаToolStripMenuItem
            // 
            this.перевіркаЧиСпаднаToolStripMenuItem.Name = "перевіркаЧиСпаднаToolStripMenuItem";
            this.перевіркаЧиСпаднаToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.перевіркаЧиСпаднаToolStripMenuItem.Text = "1. Перевірка чи спадаюча";
            this.перевіркаЧиСпаднаToolStripMenuItem.Click += new System.EventHandler(this.перевіркаЧиСпаднаToolStripMenuItem_Click);
            // 
            // точкиПеретинуЗОхToolStripMenuItem
            // 
            this.точкиПеретинуЗОхToolStripMenuItem.Name = "точкиПеретинуЗОхToolStripMenuItem";
            this.точкиПеретинуЗОхToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.точкиПеретинуЗОхToolStripMenuItem.Text = "2. Чи перетинає вісь Ох";
            this.точкиПеретинуЗОхToolStripMenuItem.Click += new System.EventHandler(this.точкиПеретинуЗОхToolStripMenuItem_Click);
            // 
            // середнєЗначенняФункціїToolStripMenuItem
            // 
            this.середнєЗначенняФункціїToolStripMenuItem.Name = "середнєЗначенняФункціїToolStripMenuItem";
            this.середнєЗначенняФункціїToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.середнєЗначенняФункціїToolStripMenuItem.Text = "3. Середнє значення функції";
            this.середнєЗначенняФункціїToolStripMenuItem.Click += new System.EventHandler(this.середнєЗначенняФункціїToolStripMenuItem_Click);
            // 
            // найбільшеВідємнеЗначенняФункціїToolStripMenuItem
            // 
            this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem.Name = "найбільшеВідємнеЗначенняФункціїToolStripMenuItem";
            this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem.Text = "4. Найбільше від\'ємне значення функції";
            this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem.Click += new System.EventHandler(this.найбільшеВідємнеЗначенняФункціїToolStripMenuItem_Click);
            // 
            // найменшеДодатнеЗначенняФункціїToolStripMenuItem
            // 
            this.найменшеДодатнеЗначенняФункціїToolStripMenuItem.Name = "найменшеДодатнеЗначенняФункціїToolStripMenuItem";
            this.найменшеДодатнеЗначенняФункціїToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.найменшеДодатнеЗначенняФункціїToolStripMenuItem.Text = "5. Найменше додатне значення функції";
            this.найменшеДодатнеЗначенняФункціїToolStripMenuItem.Click += new System.EventHandler(this.найменшеДодатнеЗначенняФункціїToolStripMenuItem_Click);
            // 
            // перевіркаЧиЗростаючаToolStripMenuItem
            // 
            this.перевіркаЧиЗростаючаToolStripMenuItem.Name = "перевіркаЧиЗростаючаToolStripMenuItem";
            this.перевіркаЧиЗростаючаToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.перевіркаЧиЗростаючаToolStripMenuItem.Text = "6. Перевірка чи зростаюча";
            this.перевіркаЧиЗростаючаToolStripMenuItem.Click += new System.EventHandler(this.перевіркаЧиЗростаючаToolStripMenuItem_Click);
            // 
            // точкаПеретинуЗОхToolStripMenuItem
            // 
            this.точкаПеретинуЗОхToolStripMenuItem.Name = "точкаПеретинуЗОхToolStripMenuItem";
            this.точкаПеретинуЗОхToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.точкаПеретинуЗОхToolStripMenuItem.Text = "7. Наближено точка перетину з Ох";
            this.точкаПеретинуЗОхToolStripMenuItem.Click += new System.EventHandler(this.точкаПеретинуЗОуToolStripMenuItem_Click);
            // 
            // симетричноВідносноОчToolStripMenuItem
            // 
            this.симетричноВідносноОчToolStripMenuItem.Name = "симетричноВідносноОчToolStripMenuItem";
            this.симетричноВідносноОчToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.симетричноВідносноОчToolStripMenuItem.Text = "8. Симетрично відносно Ох";
            this.симетричноВідносноОчToolStripMenuItem.Click += new System.EventHandler(this.симетричноВідносноОчToolStripMenuItem_Click);
            // 
            // симетричноВідносноОуToolStripMenuItem
            // 
            this.симетричноВідносноОуToolStripMenuItem.Name = "симетричноВідносноОуToolStripMenuItem";
            this.симетричноВідносноОуToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.симетричноВідносноОуToolStripMenuItem.Text = "9. Симетрично відносно Оу";
            this.симетричноВідносноОуToolStripMenuItem.Click += new System.EventHandler(this.симетричноВідносноОуToolStripMenuItem_Click);
            // 
            // уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem
            // 
            this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem.Name = "уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem";
            this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem.Size = new System.Drawing.Size(370, 26);
            this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem.Text = "10. У якому квадранті найбільше точок";
            this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem.Click += new System.EventHandler(this.уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(284, 42);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(178, 105);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series3.Legend = "Legend1";
            series3.Name = "y=sinx*e^x";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series4.Legend = "Legend1";
            series4.Name = "Series2";
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Size = new System.Drawing.Size(999, 485);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // build_function_button
            // 
            this.build_function_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.build_function_button.Location = new System.Drawing.Point(11, 179);
            this.build_function_button.Name = "build_function_button";
            this.build_function_button.Size = new System.Drawing.Size(135, 31);
            this.build_function_button.TabIndex = 5;
            this.build_function_button.Text = "build function";
            this.build_function_button.UseVisualStyleBackColor = true;
            this.build_function_button.Click += new System.EventHandler(this.build_function_button_Click);
            // 
            // b_numericUpDown
            // 
            this.b_numericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.b_numericUpDown.Location = new System.Drawing.Point(27, 147);
            this.b_numericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.b_numericUpDown.Name = "b_numericUpDown";
            this.b_numericUpDown.Size = new System.Drawing.Size(120, 26);
            this.b_numericUpDown.TabIndex = 9;
            this.b_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // b_label
            // 
            this.b_label.AutoSize = true;
            this.b_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.b_label.Location = new System.Drawing.Point(7, 147);
            this.b_label.Name = "b_label";
            this.b_label.Size = new System.Drawing.Size(18, 20);
            this.b_label.TabIndex = 8;
            this.b_label.Text = "b";
            // 
            // a_numericUpDown
            // 
            this.a_numericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.a_numericUpDown.Location = new System.Drawing.Point(27, 117);
            this.a_numericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.a_numericUpDown.Name = "a_numericUpDown";
            this.a_numericUpDown.Size = new System.Drawing.Size(120, 26);
            this.a_numericUpDown.TabIndex = 11;
            // 
            // a_label
            // 
            this.a_label.AutoSize = true;
            this.a_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.a_label.Location = new System.Drawing.Point(7, 117);
            this.a_label.Name = "a_label";
            this.a_label.Size = new System.Drawing.Size(18, 20);
            this.a_label.TabIndex = 10;
            this.a_label.Text = "a";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 602);
            this.Controls.Add(this.a_numericUpDown);
            this.Controls.Add(this.a_label);
            this.Controls.Add(this.b_numericUpDown);
            this.Controls.Add(this.b_label);
            this.Controls.Add(this.build_function_button);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.a_numericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перевіркаЧиСпаднаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem точкиПеретинуЗОхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem середнєЗначенняФункціїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найбільшеВідємнеЗначенняФункціїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найменшеДодатнеЗначенняФункціїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перевіркаЧиЗростаючаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem точкаПеретинуЗОхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem симетричноВідносноОчToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem симетричноВідносноОуToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button build_function_button;
        private System.Windows.Forms.NumericUpDown b_numericUpDown;
        private System.Windows.Forms.Label b_label;
        private System.Windows.Forms.NumericUpDown a_numericUpDown;
        private System.Windows.Forms.Label a_label;
        private System.Windows.Forms.ToolStripMenuItem уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

