﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab22V11N3
{
    public partial class Form1 : Form
    {
        FunctionOperations function;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void build_function_button_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.buildFunction(chart1);
        }
        private void перевіркаЧиСпаднаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.isDecreasing(chart1);

        }
        private void точкиПеретинуЗОхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.doesCrossOX(chart1);
        }
        private void середнєЗначенняФункціїToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.averageValue(chart1);
        }
        private void перевіркаЧиЗростаючаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.isIncreasing(chart1);
        }
        private void точкаПеретинуЗОуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.ApproximateIntersectionOX(chart1);

        }
        private void симетричноВідносноОчToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.symmetricallyOX(chart1);

        }
        private void симетричноВідносноОуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.symmetricallyOY(chart1);
        }
        private void уЯкомуКвадрантіНайбільшеТочокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.whichQuadrant(chart1);
        }
        private void найбільшеВідємнеЗначенняФункціїToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.maxNegativeValue(chart1);
        }
        private void найменшеДодатнеЗначенняФункціїToolStripMenuItem_Click(object sender, EventArgs e)
        {
            function = new FunctionOperations(Convert.ToDouble(a_numericUpDown.Value), Convert.ToDouble(b_numericUpDown.Value));
            function.minPositiveValue(chart1);
        }

        private void chart1_Click(object sender, EventArgs e)
        {
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        
    }

}
