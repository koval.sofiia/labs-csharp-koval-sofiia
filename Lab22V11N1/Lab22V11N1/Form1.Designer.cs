﻿namespace Lab22V11N1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.X_label = new System.Windows.Forms.Label();
            this.X_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.operationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskChangeElementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateAnArrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Xelements_dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.X_numericUpDown)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Xelements_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // X_label
            // 
            this.X_label.AutoSize = true;
            this.X_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.X_label.Location = new System.Drawing.Point(56, 74);
            this.X_label.Name = "X_label";
            this.X_label.Size = new System.Drawing.Size(20, 20);
            this.X_label.TabIndex = 0;
            this.X_label.Text = "X";
            // 
            // X_numericUpDown
            // 
            this.X_numericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.X_numericUpDown.Location = new System.Drawing.Point(82, 72);
            this.X_numericUpDown.Maximum = new decimal(new int[] {
            101,
            0,
            0,
            0});
            this.X_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.X_numericUpDown.Name = "X_numericUpDown";
            this.X_numericUpDown.Size = new System.Drawing.Size(91, 26);
            this.X_numericUpDown.TabIndex = 1;
            this.X_numericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.X_numericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.X_numericUpDown.ValueChanged += new System.EventHandler(this.X_numericUpDown_ValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.operationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(125, 6);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(128, 26);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // operationsToolStripMenuItem
            // 
            this.operationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.taskChangeElementsToolStripMenuItem,
            this.generateAnArrayToolStripMenuItem});
            this.operationsToolStripMenuItem.Name = "operationsToolStripMenuItem";
            this.operationsToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.operationsToolStripMenuItem.Text = "Operations";
            // 
            // taskChangeElementsToolStripMenuItem
            // 
            this.taskChangeElementsToolStripMenuItem.Name = "taskChangeElementsToolStripMenuItem";
            this.taskChangeElementsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.taskChangeElementsToolStripMenuItem.Text = "Task1";
            this.taskChangeElementsToolStripMenuItem.Click += new System.EventHandler(this.taskChangeElementsToolStripMenuItem_Click);
            // 
            // generateAnArrayToolStripMenuItem
            // 
            this.generateAnArrayToolStripMenuItem.Name = "generateAnArrayToolStripMenuItem";
            this.generateAnArrayToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.generateAnArrayToolStripMenuItem.Text = "Generate an array";
            this.generateAnArrayToolStripMenuItem.Click += new System.EventHandler(this.generateAnArrayToolStripMenuItem_Click);
            // 
            // Xelements_dataGridView
            // 
            this.Xelements_dataGridView.AllowUserToAddRows = false;
            this.Xelements_dataGridView.AllowUserToDeleteRows = false;
            this.Xelements_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Xelements_dataGridView.ColumnHeadersVisible = false;
            this.Xelements_dataGridView.Location = new System.Drawing.Point(15, 122);
            this.Xelements_dataGridView.Name = "Xelements_dataGridView";
            this.Xelements_dataGridView.RowHeadersVisible = false;
            this.Xelements_dataGridView.RowHeadersWidth = 51;
            this.Xelements_dataGridView.RowTemplate.Height = 24;
            this.Xelements_dataGridView.ShowRowErrors = false;
            this.Xelements_dataGridView.Size = new System.Drawing.Size(763, 92);
            this.Xelements_dataGridView.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Xelements_dataGridView);
            this.Controls.Add(this.X_numericUpDown);
            this.Controls.Add(this.X_label);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Lab22V11N1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.X_numericUpDown)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Xelements_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label X_label;
        private System.Windows.Forms.NumericUpDown X_numericUpDown;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taskChangeElementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateAnArrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.DataGridView Xelements_dataGridView;
    }
}

