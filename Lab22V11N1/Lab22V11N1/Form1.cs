﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Lab22V11N1
{
    public partial class Form1 : Form
    {
        UserVector vector;
        public Form1()
        {
            InitializeComponent();
        }

        private void taskChangeElementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show($"After swapping: [{string.Join(", ", vector.getResultTask1())}]");

        }


        private void X_numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            Xelements_dataGridView.ColumnCount = (int)X_numericUpDown.Value;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Xelements_dataGridView.RowCount = 1;
            Xelements_dataGridView.ColumnCount = 5;
            vector = new UserVector(Xelements_dataGridView.ColumnCount);
            vector.writeToGrid(Xelements_dataGridView);
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void generateAnArrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            vector = new UserVector(Xelements_dataGridView.ColumnCount);
            vector.writeToGrid(Xelements_dataGridView);
        }

    }
}
