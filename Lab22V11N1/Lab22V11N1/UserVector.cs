﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab22V11N1
{
    internal class UserVector
    {
        double[] vector;

        public UserVector(int length, int minValue = -10, int maxValue = 10)
        {
            vector = getRandomVector(length, minValue, maxValue);
        }
        
        public UserVector(DataGridView grid)
        {
            vector = readDataFromGrid(grid);
        }
        /// <summary>
        /// рандом
        /// </summary>
        /// <param name="length"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        double[] getRandomVector(int length, int minValue = -10, int maxValue = 10)
        {
            double[] vector = new double[length];
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < length; i++)
            {
                vector[i] = rnd.Next(minValue, maxValue);
            }
            return vector;
        }
        /// <summary>
        /// зчитує з гріда
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static double[] readDataFromGrid(DataGridView grid)
        {
            double[] vector = new double[grid.ColumnCount];
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                vector[i] = Convert.ToDouble(grid[i, 0].Value);
            }
            return vector;
        }
        /// <summary>
        /// записує у грід
        /// </summary>
        /// <param name="grid"></param>
        public void writeToGrid(DataGridView grid)
        {
            grid.ColumnCount = vector.Length;
            for (int i = 0; i < vector.Length; i++)
            {
                grid[i, 0].Value = vector[i];
            }
        }
        /// <summary>
        /// знаходження масиву відображених симетрично чисел
        /// </summary>
        /// <returns></returns>
        public double[] getResultTask1()
        {

            int mid = Convert.ToInt32(Math.Round((double)vector.Length / 2));
            MessageBox.Show($"{mid+1}, {vector[mid]}");

            for (int i = 0; i < mid; i++)
            {
                double temp = vector[i];
                vector[i] = vector[vector.Length - i - 1];
                vector[vector.Length - i - 1] = temp;
            }

            return vector;



        }
    }
}
