﻿namespace Lab21V11N2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.result_button = new System.Windows.Forms.Button();
            this.a_label = new System.Windows.Forms.Label();
            this.a_textBox = new System.Windows.Forms.TextBox();
            this.result_label = new System.Windows.Forms.Label();
            this.result_textBox = new System.Windows.Forms.TextBox();
            this.n_label = new System.Windows.Forms.Label();
            this.n_textBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // result_button
            // 
            this.result_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.result_button.Location = new System.Drawing.Point(219, 168);
            this.result_button.Name = "result_button";
            this.result_button.Size = new System.Drawing.Size(107, 29);
            this.result_button.TabIndex = 0;
            this.result_button.Text = "calculate";
            this.result_button.UseVisualStyleBackColor = true;
            this.result_button.Click += new System.EventHandler(this.result_button_Click);
            // 
            // a_label
            // 
            this.a_label.AutoSize = true;
            this.a_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.a_label.Location = new System.Drawing.Point(205, 95);
            this.a_label.Name = "a_label";
            this.a_label.Size = new System.Drawing.Size(18, 20);
            this.a_label.TabIndex = 1;
            this.a_label.Text = "a";
            // 
            // a_textBox
            // 
            this.a_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.a_textBox.Location = new System.Drawing.Point(242, 89);
            this.a_textBox.Name = "a_textBox";
            this.a_textBox.Size = new System.Drawing.Size(84, 26);
            this.a_textBox.TabIndex = 2;
            this.a_textBox.TabStop = false;
            this.a_textBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // result_label
            // 
            this.result_label.AutoSize = true;
            this.result_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.result_label.Location = new System.Drawing.Point(160, 213);
            this.result_label.Name = "result_label";
            this.result_label.Size = new System.Drawing.Size(79, 20);
            this.result_label.TabIndex = 3;
            this.result_label.Text = "result is: ";
            this.result_label.Click += new System.EventHandler(this.label1_Click);
            // 
            // result_textBox
            // 
            this.result_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.result_textBox.Location = new System.Drawing.Point(245, 210);
            this.result_textBox.Name = "result_textBox";
            this.result_textBox.ReadOnly = true;
            this.result_textBox.Size = new System.Drawing.Size(82, 26);
            this.result_textBox.TabIndex = 4;
            // 
            // n_label
            // 
            this.n_label.AutoSize = true;
            this.n_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.n_label.Location = new System.Drawing.Point(205, 131);
            this.n_label.Name = "n_label";
            this.n_label.Size = new System.Drawing.Size(18, 20);
            this.n_label.TabIndex = 5;
            this.n_label.Text = "n";
            this.n_label.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // n_textBox
            // 
            this.n_textBox.Location = new System.Drawing.Point(242, 131);
            this.n_textBox.Name = "n_textBox";
            this.n_textBox.Size = new System.Drawing.Size(84, 22);
            this.n_textBox.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-2, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(776, 70);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 419);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.n_textBox);
            this.Controls.Add(this.n_label);
            this.Controls.Add(this.result_textBox);
            this.Controls.Add(this.result_label);
            this.Controls.Add(this.a_textBox);
            this.Controls.Add(this.a_label);
            this.Controls.Add(this.result_button);
            this.Name = "Form1";
            this.Text = "Lab21V11N2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button result_button;
        private System.Windows.Forms.Label a_label;
        private System.Windows.Forms.TextBox a_textBox;
        private System.Windows.Forms.Label result_label;
        private System.Windows.Forms.TextBox result_textBox;
        private System.Windows.Forms.Label n_label;
        private System.Windows.Forms.TextBox n_textBox;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

