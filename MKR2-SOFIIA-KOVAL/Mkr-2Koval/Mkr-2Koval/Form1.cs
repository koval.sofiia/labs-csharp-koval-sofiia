﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mkr_2Koval
{
    public partial class Form1 : Form
    {
        Array localarrayuser;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 1;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = Convert.ToInt32(numericUpDown1.Value);
        }

        private void isAnArrayAnArithmeticProgrToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool result = Array.isAnArrayArithmeticProgr(dataGridView1);
            answer_textBox1.Text = result.ToString();
        }
    }
}
