﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mkr_2Koval
{
    internal class Array
    {
        static double[] localarray;


        /// <summary>
        /// Зчитує із грід поелементно і формує масив
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static double[] readVectToGrid(DataGridView grid)
        {
            double[] vect = new double[grid.ColumnCount];
            for (int j = 0; j < grid.ColumnCount; j++)
            {
                vect[j] = Convert.ToDouble(grid[j, 0].Value);
            }
            return vect;
        }

        /// <summary>
        /// Чи є арифметична прогресія
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static bool isAnArrayArithmeticProgr(DataGridView grid)
        {
            bool answer = false;
            localarray = readVectToGrid(grid);
            for (int i = 1; i < localarray.Length-1; i++)
            {
                if ((localarray[i-1] + localarray[i+1])/2 == localarray[i])
                {
                    answer = true;
                }
            }
            return answer;
        }



    }
}
