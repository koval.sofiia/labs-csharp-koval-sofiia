﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mkr2_Koval_task2
{
    [Serializable]
    internal class Kadry
    {
        public int Code { get; set; }
        public string Surname { get; set; }
        public string Factory { get; set; }
        public string Position { get; set; }
        public string Gender { get; set; }
        public double WorkExperience { get; set; }
        public double Salary { get; set; }
        public Kadry(int code, string surname, string factory, string position, string gender, double workexp, double salary)
        {
            Code = code;
            Surname = surname;
            Factory = factory;
            Position = position;
            Gender = gender;
            WorkExperience = workexp;
            Salary = salary;
        }
    }
}
