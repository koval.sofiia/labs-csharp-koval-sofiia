﻿using Project2Semester;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Mkr2_Koval_task2
{
    public partial class Form1 : Form
    {
        List<Kadry> KadryList;
        BindingSource bindingSource;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            KadryList = new List<Kadry>
            {

            };
            KadryList.Add(new Kadry(1, "Petrenko", "Car building", "Worker", "Man", 3, 10500));
            KadryList.Add(new Kadry(2, "Avdienko", "Car testing", "Tester", "Man", 2, 7800));
            KadryList.Add(new Kadry(3, "Vlastov", "Cleaning", "Cleaning", "Woman", 5, 9000));
            KadryList.Add(new Kadry(4, "Horvath", "HR", "Human resource", "Woman", 8, 17300));
            KadryList.Add(new Kadry(5, "Lachyk", "Directory", "CEO", "Man", 11, 32000));
            bindingSource = new BindingSource();
            bindingSource.DataSource = KadryList;
            dataGridView1.DataSource = bindingSource;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, KadryList);
                fs.Close();
            }

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                bindingSource.Clear();
                foreach (Kadry k in (List<Kadry>)bf.Deserialize(fs))
                    bindingSource.Add(k);
                fs.Close();
            }
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var addForm = new Addworker();
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                int code = Convert.ToInt32(addForm.codeNumericUpDown.Text);
                string newSurname = addForm.surname.Text;
                string newfactory = addForm.factory.Text;
                string newposition = addForm.position.Text;
                string newgender = addForm.gender.Text;
                double newworkexp = Convert.ToDouble(addForm.workexp.Text);
                double newsal = Convert.ToDouble(addForm.salary.Text);

                Kadry kd = new Kadry(code, newSurname, newfactory, newposition, newgender, newworkexp, newsal);

                bindingSource.Add(kd);
            }
        }
        List<Kadry> kadryFiltered;
        BindingSource BindSourceFiltered;
        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int code = Convert.ToInt32(numericUpDown1.Text);
            kadryFiltered = KadryList.FindAll(kd => kd.Code == code);
            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = kadryFiltered;
            dataGridView2.DataSource = BindSourceFiltered;
        }

        private void getAverageSalaryBetweenWomenToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string genderforfilr = "Woman";
            double averagesal = 0;
            double temp = 0;
            kadryFiltered = KadryList.FindAll(kd => kd.Gender == genderforfilr);
            foreach (var item in kadryFiltered)
            {
                temp += item.Salary;
                
            }
            averagesal = temp / kadryFiltered.Count;
            numericUpDown2.Value = (decimal)averagesal;
            

        }

        private void exportToExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ExcelConvert.saveGridToExcel(dataGridView1, saveFileDialog1.FileName, "Export_data");
            }
        }
    }
}
