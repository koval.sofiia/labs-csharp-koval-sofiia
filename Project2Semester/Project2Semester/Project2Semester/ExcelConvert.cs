﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//1. Підключити Microsoft.Office.Interop.Excel
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;

namespace Project2Semester
{
    internal class ExcelConvert
    {
        public static void saveGridToExcel(DataGridView grid, string fileName, string sheetName)
        {
            //Створюємо екземпляр додатку
            Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

            //Відображаємо додаток (не обов"язково)
            excelApp.Visible = true;

            //Задаємо кількість листів у книзі
            excelApp.SheetsInNewWorkbook = 1;

            //Створюємо робочу книгу
            Excel.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);

            //Відключаємо можливість відображення повідомлень
            excelApp.DisplayAlerts = false;

            //Отримуємо посилання на перший лист(нумерація від 1)
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);

            //Задаємо назву листа 
            sheet.Name = sheetName;

            //Записуємо дані у  лист
            int offsetRow = 1;
            //Записуємо назви стовпців
            for (int j = 0; j < grid.ColumnCount; j++)
            {
                sheet.Cells[offsetRow, j + 1] = grid.Columns[j].HeaderText;
            }

            //Записуємо дані 
            for (int i = 0; i < grid.RowCount; i++)
            {
                for (int j = 0; j < grid.ColumnCount; j++)
                    sheet.Cells[offsetRow + i + 1, j + 1] = grid[j, i].Value;
            }

            //Задаємо діапазон клітинок
            Excel.Range range1 = sheet.Range[sheet.Cells[offsetRow, 1], sheet.Cells[offsetRow, grid.ColumnCount]];
            //Ім"я шрифту
            range1.Cells.Font.Name = "Arial";

            //Розмір шрифту
            range1.Cells.Font.Size = 12;

            //Колір символів
            range1.Cells.Font.Color = ColorTranslator.ToOle(Color.White);

            //Колір фону
            range1.Interior.Color = ColorTranslator.ToOle(Color.LightPink);

            ////Зберігаємо документ
            excelApp.Application.ActiveWorkbook.SaveAs($"{fileName}.xlsx");

            //Закриваємо книгу
            workBook.Close(true);

            //Виходимо з додатку
            excelApp.Quit();
        }
    }
}
