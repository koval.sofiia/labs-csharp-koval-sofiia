﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2Semester
{
    [Serializable]
    internal class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string PhoneNumber { get; set; }
        public string Language { get;set; }
        public int TestScore { get; set; }
        public int TestScoreLast { get;set; }

        public Student(string name, string surname, int age, string phonenumber, string lang, int score, int endScore)
        {
            Name = name;
            Surname = surname;
            Age = age;
            PhoneNumber = phonenumber;
            Language = lang;
            TestScore = score;
            TestScoreLast = endScore;
        }
    }
}
