﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace Project2Semester
{
    public partial class Form1 : Form
    {
        List<Student> StudentList;
        BindingSource BindSource;
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StudentList = new List<Student>
            {

            };
            StudentList.Add(new Student("Petro", "Petrenko", 18, "0993765433", "English", 573, 600));
            StudentList.Add(new Student("Andrii", "Andriyenko", 23, "0637789865", "Deutsch", 300, 350));
            StudentList.Add(new Student("Svitlana", "Svitlychna", 53, "0506732311", "Deutsch", 189, 259));
            BindSource = new BindingSource();
            BindSource.DataSource = StudentList;
            StudentInfo.DataSource = BindSource;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, StudentList);
                fs.Close();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                BindSource.Clear();
                foreach (Student s in (List<Student>)bf.Deserialize(fs))
                    BindSource.Add(s);
                fs.Close();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void addNewStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            richTextBox1.Visible = false;
            searchResultGrid.Visible = false;
            var addForm = new AddStudentForm();
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                string newName = addForm.newNameTextBox.Text;
                string newSurname = addForm.newSurnameTextBox.Text;
                int newAge = Convert.ToInt32(addForm.ageNumericUpDown.Text);
                string newPhoneNumber = addForm.phoneNumberTextBox.Text;
                string newLanguage = addForm.langcomboBox1.Text;
                int newTestScore = Convert.ToInt32(addForm.testScoreTextBox.Text);
                int endTestScore = Convert.ToInt32(addForm.testScoreEnd.Text);

                Student st = new Student(newName, newSurname, newAge, newPhoneNumber, newLanguage, newTestScore, endTestScore);
                BindSource.Add(st);
            }
        }

        List<Student> studentFiltered;
        BindingSource BindSourceFiltered;

        private void findByNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            richTextBox1.Visible = false;
            panel2.Visible = true;
            searchResultGrid.Visible = true;
            searchResultGrid.Rows.Clear();
        }

        private void surname_textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string surname = surname_textBox1.Text;
            studentFiltered = StudentList.FindAll(st => st.Surname.Contains(surname));
            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = studentFiltered;
            searchResultGrid.DataSource = BindSourceFiltered;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            string lang = comboBox1.Text;
            studentFiltered = StudentList.FindAll(st => st.Language.Contains(lang));
            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = studentFiltered;
            searchResultGrid.DataSource = BindSourceFiltered;
        }

        private void filterByLanguageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            richTextBox1.Visible = false;
            panel1.Visible = true;
            searchResultGrid.Visible = true;
            searchResultGrid.Rows.Clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void viewStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudentInfo.Visible = true;
        }

        private void seeAProgressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = false;
            chart1.Visible = true;

            panel2.Visible = false;
            panel1.Visible = false;
            searchResultGrid.Visible = false;
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            foreach (var item in StudentList)
            {
                chart1.Series[0].Points.AddXY(item.Name, item.TestScore);
                chart1.Series[1].Points.AddXY(item.Name, item.TestScoreLast);
                //richTextBox1.AppendText($"{item.Name} {item.Surname} progress is {item.TestScoreLast - item.TestScore} points in {item.Language} test\n");
                
            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void exportToExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ExcelConvert.saveGridToExcel(StudentInfo, saveFileDialog1.FileName, "Export_data");
            }
        }

        private void importFromExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string file = ""; //variable for the Excel File Location
            DataTable dt = new DataTable(); //container for our excel data
            DataRow row;
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Check if Result == "OK".
            {
                file = openFileDialog1.FileName; //get the filename with the location of the file
                try
                {
                    //Create Object for Microsoft.Office.Interop.Excel that will be use to read excel file

                    Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

                    Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(file);

                    Microsoft.Office.Interop.Excel._Worksheet excelWorksheet = excelWorkbook.Sheets[1];

                    Microsoft.Office.Interop.Excel.Range excelRange = excelWorksheet.UsedRange;

                    int rowCount = excelRange.Rows.Count; //get row count of excel data

                    int colCount = excelRange.Columns.Count; // get column count of excel data

                    //Get the first Column of excel file which is the Column Name

                    for (int i = 1; i <= rowCount; i++)
                    {
                        for (int j = 1; j <= colCount; j++)
                        {
                            dt.Columns.Add(excelRange.Cells[i, j].Value2.ToString());
                        }
                        break;
                    }

                    //Get Row Data of Excel

                    int rowCounter; //This variable is used for row index number
                    for (int i = 2; i <= rowCount; i++) //Loop for available row of excel data
                    {
                        row = dt.NewRow(); //assign new row to DataTable
                        rowCounter = 0;
                        for (int j = 1; j <= colCount; j++) //Loop for available column of excel data
                        {
                            //check if cell is empty
                            if (excelRange.Cells[i, j] != null && excelRange.Cells[i, j].Value2 != null)
                            {
                                row[rowCounter] = excelRange.Cells[i, j].Value2.ToString();
                            }
                            else
                            {
                                row[i] = "";
                            }
                            rowCounter++;
                        }
                        dt.Rows.Add(row); //add row to DataTable
                    }

                    StudentInfo.DataSource = dt; //assign DataTable as Datasource for DataGridview

                    //close and clean excel process
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Marshal.ReleaseComObject(excelRange);
                    Marshal.ReleaseComObject(excelWorksheet);
                    //quit apps
                    excelWorkbook.Close();
                    Marshal.ReleaseComObject(excelWorkbook);
                    excelApp.Quit();
                    Marshal.ReleaseComObject(excelApp);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //if (openFileDialog1.ShowDialog() == DialogResult.OK)
                //{
                //    ExcelConvert.openGridFromExcel(StudentInfo, openFileDialog1.FileName);
                //}
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}

