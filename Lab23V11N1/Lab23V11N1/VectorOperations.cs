﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab23V11N1
{
    internal class VectorOperations
    {
        /// <summary>
        /// Генерує рандомно і формує масив з цих елементів
        /// </summary>
        /// <param name="columnsCount"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static double[] getRandomVector(int columnsCount, int minValue = -10, int maxValue = 10)
        {
            double[] vect = new double[columnsCount];
            Random rand = new Random(DateTime.Now.Millisecond);

            for (int j = 0; j < columnsCount; j++)
            {
                vect[j] = rand.Next(minValue, maxValue);
            }

            return vect;
        }

        /// <summary>
        /// Записує вектор у грід поелементно
        /// </summary>
        /// <param name="vect"></param>
        /// <param name="grid"></param>
        public static void writeVectToGrid(double[] vect, DataGridView grid)
        {

            grid.ColumnCount = vect.Length;


            for (int j = 0; j < grid.ColumnCount; j++)
            {
                grid[j, 0].Value = vect[j];
            }

        }

        /// <summary>
        /// Зчитує із грід поелементно і формує масив
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static double[] readVectToGrid(DataGridView grid)
        {
            double[] vect = new double[grid.ColumnCount];


            for (int j = 0; j < grid.ColumnCount; j++)
            {
                vect[j] = Convert.ToDouble(grid[j, 0].Value);
            }

            return vect;
        }

        /// <summary>
        /// За допомогою рандомної генерації вектора заповнює грід цими елементами
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        public static void fillGridWithRandomNumbers(DataGridView grid, int minValue = -10, int maxValue = 10)
        {
            double[] vect = VectorOperations.getRandomVector(grid.ColumnCount);
            VectorOperations.writeVectToGrid(vect, grid);
        }

        /// <summary>
        /// Зберігає вектор у файл(кожен елемент це новий рядок)
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="vect"></param>
        public static void saveVectToFile(string fileName, double[] vect)
        {
            StreamWriter sw = new StreamWriter(fileName);
            //Записує кількість стовпців

            sw.WriteLine(vect.Length);
            //Записує усі елементи вектора(кожний з нового рядка)

            for (int j = 0; j < vect.Length; j++)
            {
                sw.WriteLine(vect[j]);
            }

            sw.Close();
        }

        /// <summary>
        /// Зчитує рядки з файлу і формує їх у вектор
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static double[] readVectFromFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            int columnsCount = Convert.ToInt32(sr.ReadLine());
            double[] vect = new double[columnsCount];

            for (int j = 0; j < columnsCount; j++)
            {
                vect[j] = Convert.ToDouble(sr.ReadLine());
            }

            sr.Close();
            return vect;
        }

        /// <summary>
        /// Зчитує вектор із гріда і потім записує це у файл
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="grid"></param>
        public static void saveGridToFile(string fileName, DataGridView grid)
        {
            double[] vect = readVectToGrid(grid);
            saveVectToFile(fileName, vect);
        }

        /// <summary>
        /// Зчитує вектор із файлу і потім записує це у грід
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="grid"></param>
        public static void readGridDataFromFile(string fileName, DataGridView grid)
        {
            double[] vect = readVectFromFile(fileName);
            writeVectToGrid(vect, grid);
        }


        /// <summary>
        /// Шукає к-ть елементів які більші за мінімальне значення цього масиву
        /// </summary>
        /// <param name="grid"></param>
        public static void getAmountBiggerThanTheSmallest(DataGridView grid)
        {
            double[] vect = readVectToGrid(grid);
            int answer = 0;
            MessageBox.Show($"Min element of vector: {vect.Min()}");
            foreach (double element in vect)
            {
                if (element > vect.Min())
                {
                    answer++;
                }
            }
            MessageBox.Show($"Amount of elements that are bigger than the smallest is: {answer}");
        }

        //======================================ДЛЯ РОБОТИ З БІНАРНИМИ ФАЙЛАМИ============================================================================


        public static void saveVectToFileBinary(string fileName, double[] vect)
        {
            FileStream FS = new FileStream(fileName, FileMode.Create);
            BinaryWriter BW = new BinaryWriter(FS);

            //Записує кількість стовпців
            BW.WriteLine(vect.Length);

            //Записує усі елементи вектора(кожний з нового рядка)

            for (int j = 0; j < vect.Length; j++)
            {
                BW.WriteLine(vect[j]);
            }

            BW.Close();
            FS.Close();
        }


        public static double[] readVectFromFileBinary(string fileName)
        {
            FileStream FS1 = new FileStream(fileName, FileMode.Open);
            BinaryReader BR = new BinaryReader(FS1);

            int columnsCount = Convert.ToInt32(BR.ReadLine());
            double[] vect = new double[columnsCount];

            for (int j = 0; j < columnsCount; j++)
            {
                vect[j] = Convert.ToDouble(BR.ReadLine());
            }

            BR.Close();
            FS1.Close();
            return vect;
        }


        public static void saveGridToFileBinary(string fileName, DataGridView grid)
        {
            double[] vect = readVectToGrid(grid);
            saveVectToFileBinary(fileName, vect);
        }


        public static void readGridDataFromFileBinary(string fileName, DataGridView grid)
        {
            double[] vect = readVectFromFileBinary(fileName);
            writeVectToGrid(vect, grid);
        }
    }
}
