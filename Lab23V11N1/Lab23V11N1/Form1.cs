﻿using System;
using System.Windows.Forms;


namespace Lab23V11N1
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FilterIndex == 1)
                {
                    VectorOperations.readGridDataFromFile(openFileDialog1.FileName, dataGridView1);
                    amount_numericUpDown.Value = dataGridView1.ColumnCount;
                }
                else if (openFileDialog1.FilterIndex == 2)
                {
                    VectorOperations.readGridDataFromFileBinary(openFileDialog1.FileName, dataGridView1);
                    amount_numericUpDown.Value = dataGridView1.ColumnCount;
                }

            }


        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FilterIndex == 1)
                {
                    VectorOperations.saveGridToFile(saveFileDialog1.FileName, dataGridView1);
                }
                else if (openFileDialog1.FilterIndex == 2)
                {
                    VectorOperations.saveGridToFileBinary(saveFileDialog1.FileName, dataGridView1);
                }

            }
        }

        private void amount_numericUpDown_ValueChanged(object sender, EventArgs e)
        {

            dataGridView1.ColumnCount = (int)amount_numericUpDown.Value;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 1;
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void generateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VectorOperations.fillGridWithRandomNumbers(dataGridView1);
        }

        private void task1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VectorOperations.getAmountBiggerThanTheSmallest(dataGridView1);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
