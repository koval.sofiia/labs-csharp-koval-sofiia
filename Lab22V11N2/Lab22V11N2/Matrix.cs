﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Lab22V11N2
{
    internal class Matrix
    {
        /// <summary>
        /// матриця всередині класу
        /// </summary>
        double[,] matr;

        /// <summary>
        /// конструктор з параметрами для рандому
        /// </summary>
        /// <param name="length"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        public Matrix(int length, int minValue = -10, int maxValue = 10)
        {
            matr = getRandomMatr(length, minValue, maxValue);
        }
        /// <summary>
        /// конструктор з параметром грід
        /// </summary>
        /// <param name="grid"></param>
        public Matrix(DataGridView grid)
        {
            matr = readMatrFromGrid(grid);
        }
        /// <summary>
        /// генерує матрицю рандомно
        /// </summary>
        /// <param name="length">розмірність</param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        double[,] getRandomMatr(int length, int minValue = -10, int maxValue = 10)
        {
            double[,] matr = new double[length, length];
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    matr[i, j] = rnd.Next(minValue, maxValue);
                }
            }
            return matr;
        }
        /// <summary>
        /// зчитує з грід
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static double[,] readMatrFromGrid(DataGridView grid)
        {
            double[,] matr = new double[grid.ColumnCount, grid.RowCount];
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                for (int j = 0; j < grid.RowCount; j++)
                {
                    matr[i, j] = Convert.ToDouble(grid[i, j].Value);
                }
            }
            return matr;
        }
        /// <summary>
        /// записує до грід
        /// </summary>
        /// <param name="grid"></param>
        public void writeToGrid(DataGridView grid)
        {
            grid.ColumnCount = matr.GetLength(0);
            grid.RowCount = matr.GetLength(1);
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    grid[i, j].Value = matr[i, j];
                }
            }
        }

        /// <summary>
        /// шукає найбільший елемент головної діагоналі
        /// </summary>
        /// <returns></returns>
        public double getMaxEl()
        {
            double maxElement = matr[0, 0];
            int maxRow = 0;

            for (int i = 0; i < matr.GetLength(0); i++)
            {
                if (matr[i, i] > maxElement)
                {
                    maxElement = matr[i, i];
                    maxRow = i;
                }
            }
            MessageBox.Show($"Row number that includes max element {maxElement} on diagonal is : {maxRow + 1}");

            return maxElement;
        }

        /// <summary>
        /// знаходить добуток елементів рядка де містить найбільший елемент головної діагоналі
        /// </summary>
        /// <returns></returns>
        public double getProduct()
        {
            double product = 1;
            double maxElement = matr[0, 0];
            int maxRow = 0;

            for (int i = 0; i < matr.GetLength(0); i++)
            {
                if (matr[i, i] > maxElement)
                {
                    maxElement = matr[i, i];
                    maxRow = i;
                }
            }


            for (int i = 0; i < matr.GetLength(0); i++)
            {
                product *= matr[i, maxRow];
            }

            return product;
        }




    }
}
