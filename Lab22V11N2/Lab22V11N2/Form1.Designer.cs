﻿namespace Lab22V11N2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getMaxElToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.matrix_dataGridView = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.generate_button = new System.Windows.Forms.Button();
            this.matrix_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.max_el_label = new System.Windows.Forms.Label();
            this.max_el_textBox = new System.Windows.Forms.TextBox();
            this.getProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrix_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrix_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.operationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1198, 28);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(125, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // operationsToolStripMenuItem
            // 
            this.operationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.getMaxElToolStripMenuItem,
            this.getProductToolStripMenuItem});
            this.operationsToolStripMenuItem.Name = "operationsToolStripMenuItem";
            this.operationsToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.operationsToolStripMenuItem.Text = "Operations";
            // 
            // getMaxElToolStripMenuItem
            // 
            this.getMaxElToolStripMenuItem.Name = "getMaxElToolStripMenuItem";
            this.getMaxElToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.getMaxElToolStripMenuItem.Text = "get max el";
            this.getMaxElToolStripMenuItem.Click += new System.EventHandler(this.getMaxElToolStripMenuItem_Click);
            // 
            // matrix_dataGridView
            // 
            this.matrix_dataGridView.AllowUserToAddRows = false;
            this.matrix_dataGridView.AllowUserToDeleteRows = false;
            this.matrix_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matrix_dataGridView.ColumnHeadersVisible = false;
            this.matrix_dataGridView.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.matrix_dataGridView.Location = new System.Drawing.Point(12, 156);
            this.matrix_dataGridView.Name = "matrix_dataGridView";
            this.matrix_dataGridView.RowHeadersVisible = false;
            this.matrix_dataGridView.RowHeadersWidth = 51;
            this.matrix_dataGridView.RowTemplate.Height = 24;
            this.matrix_dataGridView.Size = new System.Drawing.Size(786, 394);
            this.matrix_dataGridView.TabIndex = 5;
            this.matrix_dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.matrix_dataGridView_CellContentClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Lab22V11N2.Properties.Resources.Screenshot_2023_04_22_143015;
            this.pictureBox1.Location = new System.Drawing.Point(206, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(943, 67);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // generate_button
            // 
            this.generate_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.generate_button.Location = new System.Drawing.Point(36, 120);
            this.generate_button.Name = "generate_button";
            this.generate_button.Size = new System.Drawing.Size(123, 30);
            this.generate_button.TabIndex = 7;
            this.generate_button.Text = "generate";
            this.generate_button.UseVisualStyleBackColor = true;
            this.generate_button.Click += new System.EventHandler(this.generate_button_Click);
            // 
            // matrix_numericUpDown
            // 
            this.matrix_numericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.matrix_numericUpDown.Location = new System.Drawing.Point(36, 87);
            this.matrix_numericUpDown.Maximum = new decimal(new int[] {
            101,
            0,
            0,
            0});
            this.matrix_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.matrix_numericUpDown.Name = "matrix_numericUpDown";
            this.matrix_numericUpDown.Size = new System.Drawing.Size(123, 26);
            this.matrix_numericUpDown.TabIndex = 9;
            this.matrix_numericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.matrix_numericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Розмірність матриці";
            // 
            // max_el_label
            // 
            this.max_el_label.AutoSize = true;
            this.max_el_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.max_el_label.Location = new System.Drawing.Point(204, 125);
            this.max_el_label.Name = "max_el_label";
            this.max_el_label.Size = new System.Drawing.Size(65, 20);
            this.max_el_label.TabIndex = 11;
            this.max_el_label.Text = "product";
            this.max_el_label.Click += new System.EventHandler(this.label2_Click);
            // 
            // max_el_textBox
            // 
            this.max_el_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.max_el_textBox.Location = new System.Drawing.Point(289, 122);
            this.max_el_textBox.Name = "max_el_textBox";
            this.max_el_textBox.ReadOnly = true;
            this.max_el_textBox.Size = new System.Drawing.Size(129, 26);
            this.max_el_textBox.TabIndex = 12;
            // 
            // getProductToolStripMenuItem
            // 
            this.getProductToolStripMenuItem.Name = "getProductToolStripMenuItem";
            this.getProductToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.getProductToolStripMenuItem.Text = "get product";
            this.getProductToolStripMenuItem.Click += new System.EventHandler(this.getProductToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1198, 562);
            this.Controls.Add(this.max_el_textBox);
            this.Controls.Add(this.max_el_label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.matrix_numericUpDown);
            this.Controls.Add(this.generate_button);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.matrix_dataGridView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Lab22V11N2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrix_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrix_numericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operationsToolStripMenuItem;
        private System.Windows.Forms.DataGridView matrix_dataGridView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem getMaxElToolStripMenuItem;
        private System.Windows.Forms.Button generate_button;
        private System.Windows.Forms.NumericUpDown matrix_numericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label max_el_label;
        private System.Windows.Forms.TextBox max_el_textBox;
        private System.Windows.Forms.ToolStripMenuItem getProductToolStripMenuItem;
    }
}

