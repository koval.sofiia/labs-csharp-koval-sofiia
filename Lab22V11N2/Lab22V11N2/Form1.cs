﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab22V11N2
{
    public partial class Form1 : Form
    {
        Matrix matr;
        public Form1()
        {
            InitializeComponent();
        }

        

        private void matrix_dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            matrix_dataGridView.ColumnCount = 3;
            matrix_dataGridView.RowCount = 3;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        

        private void generate_button_Click(object sender, EventArgs e)
        {
            matr = new Matrix(matrix_dataGridView.ColumnCount, matrix_dataGridView.RowCount);
            matr.writeToGrid(matrix_dataGridView);
        }

        

        private void getMaxElToolStripMenuItem_Click(object sender, EventArgs e)
        {
            matr = new Matrix(matrix_dataGridView);
            matr.getMaxEl();
            
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            matrix_dataGridView.ColumnCount = (int)matrix_numericUpDown.Value; 
            matrix_dataGridView.RowCount = (int)matrix_numericUpDown.Value;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void getProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            matr.getProduct();
            max_el_textBox.Text = Convert.ToString(matr.getProduct());
        }
    }
}
